const HinhNenKenh = [
  "/Dự Án Cá Nhân/SourceImage/bg1.png",
  "/Dự Án Cá Nhân/SourceImage/bg3.png",
  "/Dự Án Cá Nhân/SourceImage/bg.jpg",
  "/Dự Án Cá Nhân/SourceImage/anime2.jpg",
  "/Dự Án Cá Nhân/SourceImage/anime.jpg",
  "/Dự Án Cá Nhân/SourceImage/manga.jpg",
];

class RemoteTV {
  constructor() {
    this.status = false;
    this.tv = null;
  }
  connectTV(x) {
    this.tv = x;
  }
  switchOn() {
    this.tv.status = true;
    document.getElementById("denthuilui").innerHTML = "";
    document.getElementById("channel").value =
      "   Channel : " + this.tv.channel;
    document.getElementById("volume").value = "   Volume : " + this.tv.volume;
  }
  switchOff() {
    this.tv.status = false;
    document.getElementById("denthuilui").innerHTML =
      '<div id="denthui"></div>';
    document.getElementById("channel").value = "";
    document.getElementById("volume").value = "";
  }
  switchChannel(x) {
    let y = document.getElementById("hinhnen");
    y.src = HinhNenKenh[Math.floor(Math.random() * HinhNenKenh.length)];
    if (this.tv.status == true) {
      this.tv.channel = this.tv.channel + x;
      document.getElementById("channel").value =
        "   Channel : " + this.tv.channel;
    } else {
      alert("Turn on TV");
    }
  }
  switchVolume(x) {
    if (this.tv.status == true) {
      this.tv.volume = this.tv.volume + x;
      document.getElementById("volume").value = "   Volume : " + this.tv.volume;
    } else {
      alert("Turn on TV");
    }
  }
}
class Tivi {
  constructor() {
    this.status = false;
    this.volume = 100;
    this.channel = 1;
  }
  tvStatus() {
    if (this.status == true) {
      console.log("TV bat");
    } else {
      console.log("TV tat");
    }
  }
  tvChannel() {
    console.log(this.channel);
  }
  tvVolume() {
    console.log(this.volume);
  }
  tv_setChannel(x) {
    this.channel = x;
  }
  tv_setVolume(x) {
    this.volume = this.volume + x;
  }
}

let remote = new RemoteTV();
let tv = new Tivi();
remote.connectTV(tv);

tv.tvStatus();

tv.tvVolume();
