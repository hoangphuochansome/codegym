class Button {
  constructor() {
    this.status = false;
  }
  switchButton() {
    if (this.status == true) {
      this.status = false;
    } else {
      this.status = true;
    }
  }
  switchOff() {
    this.status = false;
  }
  switchOn() {
    this.status = true;
  }
  connectToLamp(x) {
    x.status = this.status;
  }
}
class Lamp {
  constructor() {
    this.status = false;
  }
  electricLamp() {
    if (this.status == true) {
      this.turnOn();
    } else {
      this.turnOff();
    }
  }
  turnOff() {
    alert("Den tat");
  }
  turnOn() {
    alert("Den sang");
  }
}

let button = new Button();
let lamp = new Lamp();
button.switchOff();
console.log(button);
button.connectToLamp(lamp);
lamp.electricLamp();
console.log(lamp);
