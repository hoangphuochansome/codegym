let MobilePhone = function (mobile_branch) {
  this.mobile_Name = mobile_branch;
  this.battery_Status = 100;
  this.typping_Text = "";
  this.receive_Mess = [];
  this.send_Mess = [];
  this.status = false;

  this.phoneStatus = function () {
    return this.status;
  };
  this.phoneOn = function () {
    this.status = true;
  };
  this.phoneOff = function () {
    this.status = false;
  };
  this.phoneCharge = function () {
    if (this.battery_Status < 100) {
      this.battery_Status++;
    }
  };
  this.typpingMess = function (text) {
    this.typping_Text = text;
  };
  this.receiveMess = function (text) {
    this.receive_Mess.push(text);
    this.battery_Status--;
  };
  this.sendMess = function (obj) {
    obj.receive_Mess.push(this.typping_Text);
    this.send_Mess.push(this.typping_Text);
    this.battery_Status--;
  };
  this.seensendMess = function (x) {
    let temp = "";
    for (i = 0; i < this.send_Mess.length; i++) {
      temp += "<br>" + this.send_Mess[i];
    }
    document.getElementById(x).innerHTML = temp;
    this.battery_Status--;
  };
  this.seenreceiveMess = function (x) {
    let temp = "";
    for (i = 0; i < this.receive_Mess.length; i++) {
      temp += "<br>" + (i + 1) + "&emsp;" + this.receive_Mess[i];
    }
    document.getElementById(x).innerHTML = temp;
    this.battery_Status--;
  };
};
//
let Iphone = new MobilePhone("Ai Phone");
Iphone.phoneOn();

let Android = new MobilePhone("An  đờ Roi");
Android.phoneOn();

//
showBattery();
function showBattery() {
  document.getElementById("battery_1").value = Iphone.battery_Status;
  document.getElementById("battery_2").value = Android.battery_Status;
}
function hienthi_khuvuc_IP() {
  document.getElementById("action").innerHTML =
    '<div id="phone_display_1p"></div>';
}
function hopthuden_IP() {
  hienthi_khuvuc_IP();
  Iphone.seenreceiveMess("phone_display_1p");
  showBattery();
}
function hopthudi_IP() {
  hienthi_khuvuc_IP();
  Iphone.seensendMess("phone_display_1p");
  showBattery();
}
function hienthi_soantin_IP() {
  hienthi_khuvuc_IP();
  document.getElementById("phone_display_1p").innerHTML =
    '<table><tr><th>Thông điệp muốn gửi</th></tr><tr><input type="text" id="thongdiep"></tr><input type="button" value="Send" onclick="' +
    'soantin_IP()"></tr></table>';
}

function soantin_IP() {
  let a = document.getElementById("thongdiep").value;
  Iphone.typpingMess(a);
  Iphone.sendMess(Android);
  showBattery();
  document.getElementById("thongdiep").value = "";
  console.log(Android.receive_Mess);
}
//

function hienthi_khuvuc_AR() {
  document.getElementById("action_2").innerHTML =
    '<div id="phone_display_2p"></div>';
}
function hopthuden_AR() {
  hienthi_khuvuc_AR();
  Android.seenreceiveMess("phone_display_2p");
  showBattery();
}
function hopthudi_AR() {
  hienthi_khuvuc_AR();
  Android.seensendMess("phone_display_2p");
  showBattery();
}
function hienthi_soantin_AR() {
  hienthi_khuvuc_AR();
  document.getElementById("phone_display_2p").innerHTML =
    '<table><tr><th>Thông điệp muốn gửi</th></tr><tr><input type="text" id="thongdiep2"></tr><input type="button" value="Send" onclick="' +
    'soantin_AR()"></tr></table>';
}
function soantin_AR() {
  let a = document.getElementById("thongdiep2").value;
  Android.typpingMess(a);
  Android.sendMess(Iphone);
  showBattery();
  console.log(Iphone.receive_Mess);
  document.getElementById("thongdiep2").value = "";
}
function xoa_IP() {
  document.getElementById("action").innerHTML = "";
}
function xoa_AR() {
  document.getElementById("action_2").innerHTML = "";
}
