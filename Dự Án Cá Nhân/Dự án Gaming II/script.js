var cvs = document.getElementById("canvas");
var ctx = cvs.getContext("2d");
var sonic = new Image();
var bg = new Image();
var bg2 = new Image();
var pipe = new Image();
var pipe2 = new Image();
//Source Ảnh
sonic.src = "/Dự Án Cá Nhân/SourceImage/bird1.png";
bg.src = "/Dự Án Cá Nhân/SourceImage/bg-3.jpg";
bg.src = "/Dự Án Cá Nhân/SourceImage/bg-4.jpg";
pipe.src = "/Dự Án Cá Nhân/SourceImage/pipe5.png";
pipe2.src = "/Dự Án Cá Nhân/SourceImage/pipe6.png";

var bX = 50;
var bY = 132;
var gravity = 2;
var score = 0;
// Âm thanh
var fly = new Audio();
var scor = new Audio();
var bgmusic = new Audio();
var onichan = new Audio();
fly.src = "/Dự Án Cá Nhân/SourceImage/fly.mp3";
scor.src = "/Dự Án Cá Nhân/SourceImage/ting2.mp3";
bgmusic.src = "/Dự Án Cá Nhân/SourceImage/BGmusic (mp3cut.net).mp3";
onichan.src = "/Dự Án Cá Nhân/SourceImage/onichan.mp3";
bgmusic.play();

document.addEventListener("keydown", moveUp);
function moveUp() {
  bY -= 30;
  fly.play();
}

let x = 600;
let y = 150;
let y2 = -170;
let speed = 4;

function draw() {
  bgmusic.play();
  ctx.drawImage(bg, 0, 0);
  ctx.drawImage(sonic, bX, bY);
  ctx.drawImage(pipe, x, y);
  ctx.drawImage(pipe2, x, y2);

  if (x > -250 && x < 650) {
    x = x - speed;
    ctx.drawImage(pipe, x, y);
    ctx.drawImage(pipe2, x, y2);
  } else {
    x = 600;
    y = Math.floor(Math.random() * (180 - 270) + 180);
    y2 = y - 300;
    ctx.drawImage(pipe, x, y);
    ctx.drawImage(pipe2, x, y2);
  }
  if (
    (bX + sonic.width >= x + 230 && bY >= y + 30) ||
    (bX + sonic.width >= x + 230 && bY <= y2 + 250)
  ) {
    onichan.play();
    alert("Game over");
    return false;
  }
  if (bX == x + 170) {
    scor.play();
    score++;
    console.log(score);
    if (score == 5 || score == 10 || score == 15) {
      speed = speed + 2;
    }
  }

  if (bY < 347) {
    bY += gravity;
  } else {
    onichan.play();
    alert("Game over " + score);

    return false;
  }
  ctx.fillStyle = "#fff";
  ctx.font = "20px Verdana";
  ctx.fillText("Score  : " + score, 10, cvs.height - 20);
  requestAnimationFrame(draw);
}

draw();
