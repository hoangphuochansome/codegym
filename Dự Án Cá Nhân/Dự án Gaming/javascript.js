var canvas = document.querySelector("canvas");
var ctx = (ctx_2 = canvas.getContext("2d"));

var x = 100;
var speedX = 1;
var speedY = 2;
var innerWidth = canvas.width;
var innerHeight = canvas.height;
var radius = 3;
var y = 100;

//store into memory
var x_1 = 100;
var y_1 = 140;
var bar_width = 40;
var bar_height = 5;

function animated() {
  requestAnimationFrame(animated);

  ctx_2.clearRect(0, 0, innerWidth, innerHeight);
  ctx_2.beginPath();
  ctx_2.arc(x, y, radius, 0, Math.PI * 2, false);

  ctx_2.stroke();

  if (x + radius > innerWidth || x - radius < 0) {
    speedX = -speedX;
  }
  if (y - radius < 0) {
    speedY = -speedY;
  } else if (y + radius > innerHeight) {
    alert("thua");
    return false;
  } else if (y + radius > y_1 - bar_height) {
    if (x > x_1 && x < x_1 + bar_width) {
      speedY = -speedY;
    }
  }

  x += speedX;
  y += speedY;

  ctx.fillRect(x_1, y_1, bar_width, bar_height);

  ctx.fillRect(x_1, y_1, 20, 5);
  document.onkeydown = checkKey;
}

animated();

//

let z = 0;
let z_1 = 0;

function checkKey(e) {
  if (e.keyCode == "39") {
    ctx.clearRect(x_1, y_1, bar_width, bar_height);
    x_1 += 5;
    ctx.fillRect(x_1, y_1, bar_width, bar_height);
  }
  if (e.keyCode == "37") {
    ctx.clearRect(x_1, y_1, bar_width, bar_height);
    x_1 -= 5;
    ctx.fillRect(x_1, y_1, bar_width, bar_height);
  }
}
